####################################################################################
## PROBLEM2: EIGENFACES
## Given the list of faces in the ./data/lfw2_subset/ dataset, we want to learn 
## an orthonormal basis for the given data. This is learned by obtaining the 
## eigenvectors of the inut images This technique is generally employed for 
## various purposes such as recognition, classification, compression.
## ref: https://www.youtube.com/watch?v=kw9R0nD69OU
## ref: https://en.wikipedia.org/wiki/Eigenface
##
##
## input: directory of faces in ./data/lfw2_subset/
## functions for reading images as vectors is provided
##
##
## your task: fill the following functions:
## extract_mean_stdd_faces
## normalize_faces
## compute_covariance_matrix
## compute_eigval_eigvec
##
##
## output: rmse of the test image preserving 80% energy in eigenvalues
## (lower the better)
##
##
## NOTE: all required modules are imported. DO NOT import new modules.
## NOTE: references are given intline
## tested on Ubuntu14.04, 22Oct2017, Abhilash Srikantha
####################################################################################

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import scipy.misc
import os
import time

def readImages(dirName,refSize,fExt):
    imFeatures = None
    print("check 1")
    for root, dirs, files in os.walk(dirName):
        for file in files:
            if file.endswith(fExt):
                img = np.asarray(Image.open(os.path.join(root,file)))
                # print("cehck 2")
                img = scipy.misc.imresize(img,(refSize[0],refSize[1]))
                # print("check 3")
                img = np.reshape(img,(refSize[0]*refSize[1],1))
                if imFeatures is None:
                    imFeatures = img
                else:
                    imFeatures = np.hstack((imFeatures,img))
                #img = np.reshape(img,(refSize[0],refSize[1]))
                #print("{}".format(img.shape))
                #imgplot = plt.imshow(img)
                #plt.show()
    imFeatTrain = imFeatures[:,:-1]
    imFeatTest  = imFeatures[:,imFeatures.shape[1]-1]
    imFeatTrain.shape
    return imFeatTrain, imFeatTest

def extract_mean_stdd_faces(featFaces):
    meanface = np.mean(featFaces,axis=1)
    stddfaces=np.subtract(featFaces[:,0],meanface)
    for i in range(len(featFaces[0,:])-1 ):
        stddfaces = np.vstack([stddfaces,np.subtract(featFaces[:,i+1],meanface)])
    #img = np.reshape(meanface,(60,60))
    #imgplot = plt.imshow(img)
    #plt.show()
    stddfaces = stddfaces.T
    stddfaces=np.std(featFaces,axis = 1)
    return meanface, stddfaces

def normalize_faces(featFaces, meanFaces, stddFaces):
    print("check normalize_faces")
    # std=np.std(stddFaces,axis = 1)
    normfaces = featFaces
    for i in range(len(featFaces[0,:]) ):
        normfaces[:,i] = np.subtract(featFaces[:,i],meanFaces)
    print("check 1")
    # normfaces=np.divide(normFaces[:,0],stddFaces)
    for i in range(len(featFaces[0,:]) ):
        normfaces[:,i] = np.divide(normfaces[:,i],stddFaces)
    print("check 2")
    return normfaces
    pass

def compute_covariance_matrix(normFaces):
    return np.cov(normFaces)
    pass

def compute_eigval_eigvec(covrFaces):
    print("calculating eigenvectors. this may take a while...")
    x = np.linalg.eig(covrFaces)
    return x
    pass

def show_eigvec(eigvec, cumEigval, refSize, energyTh):
    for idx in range(len(cumEigval)):
        if(cumEigval[idx] < energyTh):
            img = np.reshape(eigvec[:,idx],(refSize[0],refSize[1]))
            print("eigenvector: {} cumEnergy: {} of shape: {}".format(idx, cumEigval[idx], img.shape))
            # imgplot = plt.imshow(img)
            # plt.show()
        else:
            break
def inner_product(x,y):
    print("calculating inner product")
    return np.inner(x,y)
def reconstruct_test(featTest, meanFaces, stddFaces, eigvec, numSignificantEigval, refSize):
    # projection
    feat = np.expand_dims(featTest,1)- np.expand_dims(meanFaces,1)
    print(feat.shape)
    x = np.expand_dims(stddFaces,1)
    print(x.shape)
    norm = feat / x
    print("shape of norm is {}".format(norm.shape))
    print("shape of np.transpose(norm) is {}".format(np.transpose(norm).shape))
    # print("numSignificantEigval = {}".format(numSignificantEigval))
    # print("shape of eigvec[:,0:numSignificantEigval-1] is {}".format(eigvec[:,0:numSignificantEigval-1].shape))
    # print("check 2")
    x=np.transpose(eigvec[:,0:numSignificantEigval-1].astype(float))
    # x = x
    y=np.transpose(norm)
    # print("x={} and shape is {}".format(x,x.shape))
    # print("y={} and shape is {}".format(y,y.shape))
    #return 0
    weights = inner_product(x, y)
    print("weights = {} and shape is {}".format(weights,weights.shape))
    # reconstruction
    recon = 0*np.squeeze(feat)
    print("check 2")
    for idx,w in enumerate(weights):
        recon += w[0]*(eigvec[:,idx].astype(float))
    print("shape of recon is {} and of np.squeeze(norm) is {}".format(recon.shape,(np.squeeze(norm)).shape))
    img = np.reshape(featTest,(refSize[0],refSize[1]))
    imgplot = plt.imshow(img)
    plt.show()
    img = np.reshape(recon,(refSize[0],refSize[1]))
    imgplot = plt.imshow(img)
    plt.show()
    # rmse 
    diff = recon - np.squeeze(norm)
    # diff = recon - featTest
    print("check 4")
    rmse = np.sqrt(np.inner(np.transpose(diff) , diff) / len(recon))
    print("check 5")
    return rmse

opts = {'dirName': './lfw2_subset',
        'refSize' : [60,60],
        'fExt':'.jpg',
        'energyTh' : 0.80,
        'eps' : 1e-10,
        'inf' : 1e10}


if __name__ == "__main__":

    # time stamp
    start = time.time()

    try:
        # extract features of all faces
        os.getcwd()
        print(os.getcwd())
        featFaces, featTest = readImages(opts['dirName'],opts['refSize'],opts['fExt'])
        print("featFaces: {}, featTest {}".format(featFaces.shape, featTest.shape))
        print("data read successfully")
        # extract mean face
        meanFaces, stddFaces = extract_mean_stdd_faces(featFaces)
        stddFaces = stddFaces+opts['eps']
        print("meanFaces: {}, stddFaces: {}".format(meanFaces.shape, stddFaces.shape))
        print("meanfaces and stddfaces extracted successfully")
        # normalize faces
        # ref: https://stats.stackexchange.com/questions/69157/why-do-we-need-to-normalize-data-before-principal-component-analysis-pca
        # ref: https://stackoverflow.com/questions/23047235/matlab-how-to-normalize-image-to-zero-and-unit-variance
        normFaces = normalize_faces(featFaces, meanFaces, stddFaces)
        print("normFaces: {}".format(normFaces.shape))
        print("faces normalized successfully")
        # covariance matrix
        covrFaces = compute_covariance_matrix(normFaces) + opts['eps']
        print("covrFaces: {}".format(covrFaces.shape))
        print("covariance matrix built successfully")
        # eigenvalues and eigenvectors
        eigval, eigvec = compute_eigval_eigvec(covrFaces)
        print("eigval: {} eigvec: {}".format(eigval.shape, eigvec.shape))
        print("eigenvalues and eigenvectors created successfully")
        # find number of eigvenvalues cumulatively smaller than energhTh
        cumEigval = np.cumsum(eigval / sum(eigval))
        numSignificantEigval = next(i for i,v in enumerate(cumEigval) if v > opts['energyTh'])
        #print(numSignificantEigval)
        #print(cumEigval.shape)
        print("cumulative sum generated successfully")
        # show top 90% eigenvectors
        # call this function to visualize eigenvectors
        show_eigvec(eigvec, cumEigval, opts['refSize'],opts['energyTh'])
        print("reconstrucitng input images on the eigenfaces...")
        # reconstruct test image
        rmse = reconstruct_test(featTest, meanFaces, stddFaces, eigvec, numSignificantEigval, opts['refSize'])
        print('#eigval preserving {}% of energy: {}'.format(100*opts['energyTh'],numSignificantEigval))
    except:
        rmse = opts['inf']
        print("exception occurred")
    
    # final output
    print('time elapsed: {}'.format(time.time() - start))
    print('rmse on compressed test image: {} (lower the better)'.format(rmse))
